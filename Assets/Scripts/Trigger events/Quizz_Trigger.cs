﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Quizz_Trigger : MonoBehaviour
{
    public GameObject quizz;

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            quizz.SetActive(true);
        }
    }
}
