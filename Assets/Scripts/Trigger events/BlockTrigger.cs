﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockTrigger : MonoBehaviour
{
    [SerializeField] private GameObject outer;
    [SerializeField] private GameObject inner;
    [SerializeField] private GameObject exit;
    [SerializeField] private GameObject hint;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            outer.SetActive(false);
            StartCoroutine(WaitForSceneLoad());
        }
    }

    private IEnumerator WaitForSceneLoad()
    {
        yield return new WaitForSeconds(0.7f);
        inner.SetActive(true);
        gameObject.SetActive(false);
        exit.SetActive(true);
        hint.SetActive(true);
    }
}
