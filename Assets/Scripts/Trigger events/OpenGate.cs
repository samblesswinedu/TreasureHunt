﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenGate : MonoBehaviour
{
    private Animator animcontrol;
    [SerializeField]
    private GameObject gate;
    private void Start()
    {
        animcontrol = gate.GetComponent<Animator>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if(other.tag=="Player")
        {
            animcontrol.SetBool("open", true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if(other.tag=="Player")
        {
            animcontrol.SetBool("open", false);
        }
    }
}
