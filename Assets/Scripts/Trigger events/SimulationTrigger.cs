﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimulationTrigger : MonoBehaviour
{
    [SerializeField] private GameObject txt1;
    [SerializeField] private GameObject score;
    [SerializeField] private GameObject link;
    private int cnt;

    private void Update()
    {
        cnt = score.GetComponent<ComponentCount>().count;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player" && cnt >= 6)
        {
            link.SetActive(true);
        }

        else if (other.tag == "Player" && cnt<6)
        {
            txt1.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            txt1.SetActive(false);
            link.SetActive(false);
        }
    }
}
