﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExitTrigger : MonoBehaviour
{
    [SerializeField] private GameObject outer;
    [SerializeField] private GameObject inner;
    [SerializeField] private GameObject entry;
    [SerializeField] private GameObject hint;


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            inner.SetActive(false);
            outer.SetActive(true);
            gameObject.SetActive(false);
            hint.SetActive(false);
            entry.SetActive(true); 
        }
    }
}
