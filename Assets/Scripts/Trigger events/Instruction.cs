﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Instruction : MonoBehaviour
{
    public GameObject txt;
    public GameObject instruction;
    public GameObject count;


    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            txt.SetActive(false);
            instruction.SetActive(true);
            count.SetActive(false);
            Time.timeScale = 0;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            txt.SetActive(true);
        }
        
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "Player")
        {
            txt.SetActive(false);
        }
    }

    public void exit()
    {
        instruction.SetActive(false);
        count.SetActive(true);
        Time.timeScale = 1;
    }
}
