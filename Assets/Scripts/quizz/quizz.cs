﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class quizz : MonoBehaviour
{
    public GameObject quizztrigger;
    public GameObject timer;
    [SerializeField]
    private GameObject collectable;

    [SerializeField]
    private GameObject nextqs;
        
    private CountDownTimer countdowntimer;
    [SerializeField]
    private TMP_Text counttext;
    private ComponentCount componentcount;

    private void Start()
    {
        countdowntimer = timer.GetComponent<CountDownTimer>();
        componentcount = counttext.GetComponent<ComponentCount>();
    }

    public void crct()
    {
        componentcount.addcomponent();
        nextqs.SetActive(true);
        Destroy(gameObject);
        collectable.SetActive(true);
        Destroy(collectable, 1f);
        Destroy(quizztrigger);
    }

   

    public void wrong()
    {
        timer.SetActive(true);
        countdowntimer.StartTimer(20);
    }
}
