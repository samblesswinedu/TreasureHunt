﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CountDownTimer : MonoBehaviour
{
    public TMP_Text counttimertext;

    public void StartTimer(int timer)
    {
        StartCoroutine(Startcount(timer));
        Debug.Log(timer);
    }

    IEnumerator Startcount(int timer)
    {
        while(timer>=0)
        {
            counttimertext.text = timer.ToString();
            yield return new WaitForSeconds(1f);
            timer--;
        }
        gameObject.SetActive(false);
    }
}
