﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ComponentCount : MonoBehaviour
{
    public int count=0;
    private TMP_Text counttext;

    private void Start()
    {
        counttext=gameObject.GetComponent<TMP_Text>();
    }
    public void addcomponent()
    {
        count++;
        counttext.text = count.ToString();
    }
}
